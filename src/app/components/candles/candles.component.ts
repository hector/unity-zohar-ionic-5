import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BuyCandlePage } from 'src/app/pages/buy-candle/buy-candle.page';
import { ZoharService } from 'src/app/services/zohar.service';

@Component({
  selector: 'app-candles',
  templateUrl: './candles.component.html',
  styleUrls: ['./candles.component.scss'],
})
export class CandlesComponent implements OnInit {
  candleType = 'refua';
  candles = [];
  constructor(
    private zoharService: ZoharService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.getCandlesFromServer();
  }

  getCandlesFromServer(){
    this.zoharService.getCandles().subscribe((candles: any) => {
      const newRefua = [];
      Object.keys(candles.candles.refua).forEach(key => {
        newRefua.push(candles.candles.refua[key]);
      });
      this.candles = this.candles.concat(newRefua);
      const newBless = [];
      Object.keys(candles.candles.bless || {}).forEach(key => {
        newBless.push(candles.candles.bless[key]);
      });
      this.candles = this.candles.concat(newBless);

      const newWish = [];
      Object.keys(candles.candles.wish || {}).forEach(key => {
        newWish.push(candles.candles.wish[key]);
      });
      this.candles = this.candles.concat(newWish);
    }, err => {
      this.candles = [];
    });
  }

  async openBuyCandle(){
    const modal = await this.modalController.create({
      component: BuyCandlePage,
      cssClass: 'my-custom-class'
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.buyed){
      this.getCandlesFromServer();
    }
  }

}
