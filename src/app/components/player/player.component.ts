import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import {Howl, Howler} from 'howler';
@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnChanges {

  player: any;
  playing = false;
  trackProgress = '0';
  trackDuration = 0;
  timeOut = [];
  url = null;
  paused = false;
  @Input() audioUrl: any;
  constructor() {}

  ngOnChanges(changes: SimpleChanges) {
    const data = changes.audioUrl.currentValue || null;
    if (data){
      console.log('------------------', data);
      this.url = data;
    }
  }

  start(url: any){
    console.log('--------------- START', url);
    Howler.stop();
    if (this.player){
      this.player.stop();
    }
    this.player = new Howl({
      src: url,
      html5: true,
      onplay: () => {
        this.playing = true;
        this.paused = false;
        this.updateProgress();
      },
      onpause: () => {
        this.paused = true;
      },
      onload: () => {
        console.log('loaded---------------------------------->');
        this.trackDuration = this.player.duration();
      },
      onloaderror: (err, errr) => {
        console.log('loadederror---------------------------------->');
        console.error(err);
        console.log(errr);
        console.log('loadederror---------------------------------->');
      },
      onunlock: () => {
        console.log('onunlock---------------------------------->');
      },
      onstop: () => {
        console.log('onestop---------------------------------->');
        this.playing = false;
        this.paused = false;
        this.trackProgress = '0';
        this.timeOut.forEach((to) => {
          clearTimeout(to);
        });
      },
      onend: () => {
        this.playing = false;
        this.paused = false;
        this.trackProgress = '0';
        this.timeOut.forEach((to) => {
          clearTimeout(to);
        });
      }
    });
    this.player.play();
  }

  togglePlayer(pause: any){
    if (!pause && !this.paused){
        this.start(this.url);
        this.playing = true;
        return;
    }
    this.playing = !pause;
    if (pause){
      this.player.pause();
    }else{
      this.player.play();
    }
  }

  stop(){
    if (this.player){
      this.player.stop();
      this.playing = false;
    }
  }

  updateProgress(){
    const seek = this.player.seek();
    this.trackProgress = (seek / this.trackDuration).toFixed(2);
    const to = setTimeout(() => {
      this.updateProgress();
    }, 1000);
    this.timeOut.push(to);
  }
}
