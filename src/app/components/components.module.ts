import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipes.module';
import { CandlesComponent } from './candles/candles.component';
import { PlayerComponent } from './player/player.component';
import { ZoharLoaderComponent } from './zohar-loader/zohar-loader.component';


@NgModule({
    declarations: [
        ZoharLoaderComponent,
        CandlesComponent,
        PlayerComponent
    ],
    imports: [IonicModule, TranslateModule, CommonModule, FormsModule, PipesModule],
    exports: [
        ZoharLoaderComponent,
        CandlesComponent,
        PlayerComponent
    ]
})
export class ComponentsModule { }
