import { NgModule } from '@angular/core';
import { CandleFilter, HasCandle, ZoharPipe } from './zohar.pipe';
@NgModule({
    declarations: [ZoharPipe, CandleFilter, HasCandle],
    imports: [],
    exports: [ZoharPipe, CandleFilter, HasCandle]
})
export class PipesModule {

}
