import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'zohar'
})
export class ZoharPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return null;
  }

}

@Pipe({
  name: 'candleFilter'
})
export class CandleFilter implements PipeTransform{
  transform(items: unknown[], ...args: unknown[]){
    const type = args[0];
    return items.filter((item: any) => item.Candle.sponsor_type === type);
  }
}


@Pipe({
  name : 'hasCandle'
})
export class HasCandle implements PipeTransform{
  transform(items: unknown[], ...args: unknown[]){
      const type = args[0];
      const has = items.filter((item: any) => item.Candle.sponsor_type === type).length;
      if (has === 0){
          return false;
      }
      return true;
  }
}
