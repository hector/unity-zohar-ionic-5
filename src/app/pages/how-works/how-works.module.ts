import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HowWorksPageRoutingModule } from './how-works-routing.module';

import { HowWorksPage } from './how-works.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    HowWorksPageRoutingModule
  ],
  declarations: [HowWorksPage]
})
export class HowWorksPageModule {}
