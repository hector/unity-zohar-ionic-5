import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-how-works',
  templateUrl: './how-works.page.html',
  styleUrls: ['./how-works.page.scss'],
})
export class HowWorksPage implements OnInit {
  txt: SafeHtml;
  constructor(
    private sanitizer: DomSanitizer,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.translate.get('HOW_IT_WORKS_HTML').subscribe((res: string) => {
      this.txt = this.sanitizer.bypassSecurityTrustHtml(res);
    });
  }

}
