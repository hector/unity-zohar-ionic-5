import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HowWorksPage } from './how-works.page';

const routes: Routes = [
  {
    path: '',
    component: HowWorksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HowWorksPageRoutingModule {}
