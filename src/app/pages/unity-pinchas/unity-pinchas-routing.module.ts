import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnityPinchasPage } from './unity-pinchas.page';

const routes: Routes = [
  {
    path: '',
    component: UnityPinchasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnityPinchasPageRoutingModule {}
