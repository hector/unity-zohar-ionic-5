import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnityPinchasPageRoutingModule } from './unity-pinchas-routing.module';

import { UnityPinchasPage } from './unity-pinchas.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    UnityPinchasPageRoutingModule
  ],
  declarations: [UnityPinchasPage]
})
export class UnityPinchasPageModule {}
