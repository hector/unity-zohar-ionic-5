import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoharByBooksPage } from './zohar-by-books.page';

const routes: Routes = [
  {
    path: '',
    component: ZoharByBooksPage
  },
  {
    path: ':id',
    loadChildren: () => import('./zohar-book/zohar-book.module').then( m => m.ZoharBookPageModule)
  },
  {
    path: ':id/:paragraphId',
    loadChildren: () => import('./zohar-book/zohar-book.module').then( m => m.ZoharBookPageModule)
  },
  {
    path: 'bookmarks',
    loadChildren: () => import('./bookmarks/bookmarks.module').then( m => m.BookmarksPageModule),
    data: {
      preload: true
    }
  },
  {
    path: 'bookmark',
    loadChildren: () => import('./bookmark/bookmark.module').then( m => m.BookmarkPageModule),
    data: {
      preload: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoharByBooksPageRoutingModule {}
