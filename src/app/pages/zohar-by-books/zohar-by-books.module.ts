import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoharByBooksPageRoutingModule } from './zohar-by-books-routing.module';

import { ZoharByBooksPage } from './zohar-by-books.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    ZoharByBooksPageRoutingModule
  ],
  declarations: [ZoharByBooksPage]
})
export class ZoharByBooksPageModule {}
