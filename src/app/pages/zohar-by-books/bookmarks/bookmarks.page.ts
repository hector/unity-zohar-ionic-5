import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { BookmarkService } from 'src/app/services/bookmark.service';

@Component({
  selector: 'app-bookmarks',
  templateUrl: './bookmarks.page.html',
  styleUrls: ['./bookmarks.page.scss'],
})
export class BookmarksPage implements OnInit {

  bookmarks = [];
  lastRead: any;
  constructor(
    private modalCtrl: ModalController,
    private bookmarkService: BookmarkService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.loadBookmarks();
  }

  loadBookmarks(){
    this.bookmarkService.getBookmarks().then((bookmarks: any) => {
      this.bookmarks = bookmarks;
    });
    this.bookmarkService.getLastRead().then((bookmark: any) => {
      console.log(bookmark);
      this.lastRead = bookmark;
    });
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

  goToParagraph(bookId: any, parNum: any){
    this.router.navigate(['zohar-by-books', bookId, parNum]);
    this.dismiss();
  }

  deleteBookmark(id: any){
    this.bookmarkService.removeBookmark(id).then((bookmark: any) => {
      this.loadBookmarks();
      console.log('bookmark remove');
    });
  }

}
