import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BookmarkService } from 'src/app/services/bookmark.service';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.page.html',
  styleUrls: ['./bookmark.page.scss'],
})
export class BookmarkPage implements OnInit {
  @Input() zohar: any;
  annotation = '';
  bookmark: any;
  constructor(private modalCtrl: ModalController, private bookmarkService: BookmarkService) { }

  ngOnInit() {
    this.getBookmark();
  }
  dismiss(option: string) {
    this.modalCtrl.dismiss({
      option
    });
  }

  saveBookmark(){
    this.bookmarkService.saveBookMark({
      zohar_id: this.zohar.Zohar.id,
      annotation: this.annotation,
      parasha: this.zohar.Parasha.name,
      parasha_hebrew: this.zohar.Parasha.name_hebrew,
      par_num: this.zohar.Zohar.paragraph_num,
      parasha_id: this.zohar.Parasha.id
    });
    this.dismiss('saved');
  }

  async getBookmark(){
    this.bookmarkService.getBookmark(this.zohar.Zohar.id).then((bookmark) => {
      if (bookmark){
        this.bookmark = bookmark;
        this.annotation = bookmark.annotation;
      }
    });
  }

  removeBookmark(){
    this.bookmarkService.removeBookmark(this.zohar.Zohar.id).then((bookmark: any) => {
      this.annotation = '';
      this.dismiss('deleted');
      console.log('bookmark remove');
    });
  }

}
