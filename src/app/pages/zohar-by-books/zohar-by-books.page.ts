import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BookmarkService } from 'src/app/services/bookmark.service';
const { Storage } = Plugins;
import { ZoharService } from '../../services/zohar.service';
import { BookmarksPage } from './bookmarks/bookmarks.page';
@Component({
  selector: 'app-zohar-by-books',
  templateUrl: './zohar-by-books.page.html',
  styleUrls: ['./zohar-by-books.page.scss'],
})
export class ZoharByBooksPage implements OnInit {
  loadingBooks = false;
  books = [];
  constructor(
    private zoharService: ZoharService,
    private modalController: ModalController,
    private bookmarksService: BookmarkService,
    private translate: TranslateService,
    private alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter(){
    this.getBooks();
    this.getBookmarksFromServer();
  }

  getBooks(){
    this.loadingBooks = true;
    Storage.get({ key: 'allBooks' }).then(res => {
      this.books = JSON.parse(res.value) || [];
      if (this.books.length > 0) {
        this.loadingBooks = false;
        return;
      }
      this.getFromServer();
    });
  }

  getFromServer(){
    this.zoharService.getBooks().subscribe((books: any) => {
      this.books = books.parashot;
      Storage.set({
        key: 'allBooks',
        value: JSON.stringify(books.parashot)
      });
      this.loadingBooks = false;
    }, err => {
      this.loadingBooks = false;
      this.books = [];
    });
  }

  getBookmarksFromServer(){
    this.bookmarksService.getBookmarksFromServer().subscribe();
  }

  async viewBookmarks(){
    let result: any = await Storage.get({ key: 'user_token' });
    result = result.value;
    if (!result){
      console.log('loggg a');
      const texts =  this.translate.instant([
        'ACTIVATE_PERSONAL_STATISTICS',
        'GO_HOME_STATISTICS',
        'CANCEL',
        'GO_HOME'
      ]);

      const alert = await this.alertController.create({
        cssClass: 'alert-statistics',
        header: texts.ACTIVATE_PERSONAL_STATISTICS,
        message: texts.GO_HOME_STATISTICS,
        buttons: [
          {
            text: texts.CANCEL,
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('cancel Ok');
            }
          },
          {
            text: texts.GO_HOME,
            cssClass: 'confirm',
            handler: () => {
              this.router.navigate(['home']);
            }
          }
        ]
      });
      await alert.present();
      return;
    }
    const modal = await this.modalController.create({
      component: BookmarksPage
    });
    await modal.present();
  }
}
