import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoharBookPageRoutingModule } from './zohar-book-routing.module';

import { ZoharBookPage } from './zohar-book.page';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    ZoharBookPageRoutingModule
  ],
  declarations: [ZoharBookPage]
})
export class ZoharBookPageModule {}
