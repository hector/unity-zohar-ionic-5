import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ZoharService } from 'src/app/services/zohar.service';
import { Plugins } from '@capacitor/core';
import { AlertController, IonContent, IonRange, ModalController } from '@ionic/angular';
const { Storage } = Plugins;
import {Howler} from 'howler';
import { BookmarksPage } from '../bookmarks/bookmarks.page';
import { BookmarkPage } from '../bookmark/bookmark.page';
import { BookmarkService } from 'src/app/services/bookmark.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'app-zohar-book',
  templateUrl: './zohar-book.page.html',
  styleUrls: ['./zohar-book.page.scss'],
})
export class ZoharBookPage implements OnInit {

  zohars = [0, 0, 0];
  parasha = {};
  books = [];
  bookId = null;
  language = 'hebrew';
  fontSize = 14;
  showTransComment = false;
  loadingZohars = false;
  loadingBooks = false;
  parcount = 1;
  currentPar: any = 1;
  bookmarked = [];
  appLanguage = 'en';
  showCommentary = false;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild('range', { static: false }) range: IonRange;
  constructor(
    private route: ActivatedRoute,
    private zoharService: ZoharService,
    private modalController: ModalController,
    private bookmarkService: BookmarkService,
    private translate: TranslateService,
    private router: Router,
    private alertController: AlertController,
    private sanitizer: DomSanitizer
    ) { }

  async ngOnInit() {
    console.log('init');
    this.loadingBooks = true;
    this.appLanguage = this.translate.currentLang;
    const { value } = await Storage.get({ key: 'allBooks' });
    this.books = JSON.parse(value) || [];
    Storage.get({ key: 'fontSize' }).then(res => {
      this.fontSize = parseInt(res.value, 0) || this.fontSize;
    });
    Storage.get({ key: 'reading_language' }).then(res => {
      this.language = res.value || this.language;
    });
    Storage.get({ key: 'show_transliteration' }).then((res: any) => {
      this.showTransComment = (res.value === 'true') || this.showTransComment;
    });
    Storage.get({ key: 'show_commentary' }).then((res: any) => {
      this.showCommentary = (res.value === 'true') || this.showCommentary;
    });

    this.route.paramMap.subscribe(queryParams => {
      this.bookId = queryParams.get('id');
      this.currentPar = queryParams.get('paragraphId') || '1';
      this.getBookParagraphs(this.bookId, this.currentPar);
    });
    this.loadingBooks = false;
  }
  ionViewWillEnter(){
    console.log('enerrrr');
  }
  ionViewWillLeave(){
    Howler.stop();
  }
  viewTransliteration(event: any){
    Storage.set({
      key: 'show_transliteration',
      value: event.detail.checked
    });
  }

  viewTCommentary(){
    this.showCommentary = !this.showCommentary;
    Storage.set({
      key: 'show_commentary',
      value: (this.showCommentary).toString()
    });
  }

  getBookParagraphs(book = '1', paragraph = '1'){
    this.loadingZohars = true;
    this.zoharService.goToParagraph(book, paragraph).subscribe(res => {
        let zo = this.proccessParagraphs(res.parashot.paragraphs || []);
        zo = this.proccessCommentary(zo);
        this.zohars = zo;
        const zoIds = zo.map((zohar: any) => {
          return zohar.Zohar.id;
        });
        this.parasha = res.parashot.Parasha || {};
        this.parcount = res.parashot.Parasha.parcount;
        this.currentPar = parseInt(paragraph, 0);
        this.content.scrollToTop(500);
        this.bookmarkService.isBookmarked(zoIds).then((bookmarked: any) => {
          console.log(bookmarked);
          this.bookmarked = bookmarked;
        });
        this.loadingZohars = false;
        this.saveLastReaded();
    });
  }
  proccessParagraphs(zohars: any){
    zohars = zohars.map((par: any) => {
      if (par.Zohar.text_hebrew !== 'null' && par.Zohar.text_hebrew !== '') {
        par.Zohar.text_hebrew = '<span class=\'item-note\'>[' +  par.Zohar.paragraph_num + ']</span>'  + par.Zohar.text_hebrew;
      }else{
        par.Zohar.text_hebrew = null;
      }
      if (par.Zohar.text_aramaic !== 'null' && par.Zohar.text_aramaic !== '') {
        par.Zohar.text_aramaic = '<span class=\'item-note\'>[' +  par.Zohar.paragraph_num + ']</span>'  + par.Zohar.text_aramaic;
      }else{
        par.Zohar.text_aramaic = null;
      }
      /*par.ZoharCommentary = par.ZoharCommentary.map((item: any) => {

      }); */

      return par;
    });
    return zohars;
  }

  fontDown() {
    if (this.fontSize > 14) {
       this.fontSize  = this.fontSize - 2;
       Storage.set({
        key: 'fontSize',
        value: this.fontSize.toString()
      });
    }
  }

  fontUp() {
    if (this.fontSize < 36) {
      this.fontSize  = this.fontSize + 2;
      Storage.set({
        key: 'fontSize',
        value: this.fontSize.toString()
      });
   }
  }

  languageChanged(ev: any) {
    const lang = ev.detail.value ?? 'hebrew';
    Storage.set({
      key: 'reading_language',
      value: lang
    });
  }

  changeBook(ev: any){
    // tslint:disable-next-line:triple-equals
    if (ev.detail.value ==  this.bookId){
      return;
    }
    this.getBookParagraphs(ev.detail.value);
    this.bookId = ev.detail.value;
  }

  changeParagraph(){
    this.currentPar = this.range.value;
    this.getBookParagraphs(this.bookId, this.range.value.toString());
  }

  goNextPrevParagraphs(reverse: any = 0){
    const par: any = reverse ? this.zohars[0] : this.zohars[this.zohars.length - 1];

    const paragraphId = par.Zohar.id || null;
    console.log(paragraphId);
    if (!paragraphId) { return; }
    this.loadingZohars = true;
    this.zoharService.getNextParagraphs(paragraphId, reverse).subscribe((res: any) => {
      let zo = this.proccessParagraphs(res.parashot.paragraphs || []);
      zo = this.proccessCommentary(zo);
      const zoIds = zo.map((zohar: any) => {
        return zohar.Zohar.id;
      });
      this.zohars = zo;
      this.parasha = res.parashot.Parasha || {};
      this.bookId = res.parashot.Parasha.id;
      this.parcount = res.parashot.Parasha.parcount;
      const cuZohar: any  = this.zohars[0];
      this.currentPar = cuZohar.Zohar.paragraph_num;
      this.bookmarkService.isBookmarked(zoIds).then((bookmarked: any) => {
        console.log(bookmarked);
        this.bookmarked = bookmarked;
      });
      this.content.scrollToTop(500);
      this.loadingZohars = false;
      this.saveLastReaded();
    });
  }

  saveLastReaded(){
    let last: any = this.zohars[this.zohars.length - 1];
    last = {
      zohar_id: last.Zohar.id,
      parasha: last.Parasha.name,
      parasha_hebrew: last.Parasha.name_hebrew,
      par_num: last.Zohar.paragraph_num,
      parasha_id: last.Parasha.id
    };
    this.bookmarkService.saveLastRead(last);
  }

  async viewBookmarks(){
    const canBook = await this.checkBookmarksActivated();
    if (!canBook){
      return;
    }
    const modal = await this.modalController.create({
      component: BookmarksPage
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    const zoIds = this.zohars.map((zo: any) => {
      return zo.Zohar.id;
    });
    this.bookmarkService.isBookmarked(zoIds).then((bookmarked: any) => {
      console.log(bookmarked);
      this.bookmarked = bookmarked;
    });
  }

  async openBookmark(zohar: any){
    const canBook = await this.checkBookmarksActivated();
    if (!canBook){
      return;
    }
    const modal = await this.modalController.create({
      component: BookmarkPage,
      componentProps: {
        zohar
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    const zoIds = this.zohars.map((zo: any) => {
      return zo.Zohar.id;
    });
    setTimeout(() => {
      this.bookmarkService.isBookmarked(zoIds).then((bookmarked: any) => {
        console.log(bookmarked);
        this.bookmarked = bookmarked;
      });
    }, 1000);
  }

 async checkBookmarksActivated(){
    let result: any = await Storage.get({ key: 'user_token' });
    result = result.value;
    if (!result){
      const texts =  this.translate.instant([
        'ACTIVATE_PERSONAL_STATISTICS',
        'GO_HOME_STATISTICS',
        'CANCEL',
        'GO_HOME'
      ]);

      const alert = await this.alertController.create({
        cssClass: 'alert-statistics',
        header: texts.ACTIVATE_PERSONAL_STATISTICS,
        message: texts.GO_HOME_STATISTICS,
        buttons: [
          {
            text: texts.CANCEL,
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('cancel Ok');
            }
          },
          {
            text: texts.GO_HOME,
            cssClass: 'confirm',
            handler: () => {
              this.router.navigate(['home']);
            }
          }
        ]
      });
      await alert.present();
      return false;
    }
    return true;
  }

  proccessCommentary(zohars: any){
    zohars = zohars.map((par: any) => {
      let translation: SafeHtml = '';
      const comments = [];
      par.ZoharCommentary = par.ZoharCommentary.map((comment: any) => {
        if (comment.lang === this.appLanguage){
          if (!translation && comment.zohartranslation){
            translation = this.sanitizer.bypassSecurityTrustHtml(comment.zohartranslation);
          }
          if (comment.commentary){
            comments.push(comment.commentary);
          }
        }
      });
      par.Zohar.translation = translation;
      par.Zohar.comments = comments;
      return par;
    });
    console.log(zohars);
    return zohars;
  }
}
