import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoharBookPage } from './zohar-book.page';

const routes: Routes = [
  {
    path: '',
    component: ZoharBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoharBookPageRoutingModule {}
