import { Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import MarkerClusterer from '@googlemaps/markerclustererplus';
import { ZoharService } from 'src/app/services/zohar.service';
declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  gMap: any;

  @ViewChild('map' , {read: ElementRef, static: false}) map: ElementRef;

  constructor(private zoharService: ZoharService) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
      this.showMap();
  }

  showMap(){
    const mapOptions = {
      center: new google.maps.LatLng(1, 1),
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false
    };
    this.gMap = new google.maps.Map(this.map.nativeElement, mapOptions);
    this.zoharService.getVisitors().subscribe((res: any) => {
      const markers = res.visitors.map((location: any) => {
        if (location.ZoharCycle.lat && location.ZoharCycle.lng){
          return new google.maps.Marker({
            position: {
              lat: parseFloat(location.ZoharCycle.lat),
              lng: parseFloat(location.ZoharCycle.lng)
            }
          });
        }
      });
      // @ts-ignore MarkerClusterer defined via script
      // tslint:disable-next-line:no-unused-expression
      new MarkerClusterer(this.gMap , markers, {
        imagePath:
          'https://unityzohar.com/images/m',
      });
    });
  }

}
