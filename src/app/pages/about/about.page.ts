import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  txt: SafeHtml;
  constructor(
    private sanitizer: DomSanitizer,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.translate.get('ABOUT_UZ_TEXT').subscribe((res: string) => {
      this.txt = this.sanitizer.bypassSecurityTrustHtml(res);
    });
  }
}
