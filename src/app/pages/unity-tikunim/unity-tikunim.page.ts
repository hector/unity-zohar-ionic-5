import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Plugins } from '@capacitor/core';
import { IonContent } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
const { Storage } = Plugins;
import { ZoharService } from '../../services/zohar.service';

@Component({
  selector: 'app-unity-tikunim',
  templateUrl: './unity-tikunim.page.html',
  styleUrls: ['./unity-tikunim.page.scss'],
})

export class UnityTikunimPage implements OnInit {

  zohars = null;
  language = 'hebrew';
  fontSize = 14;
  showTransComment = false;
  nextAvailable = false;
  loadingZohars = false;
  appLanguage = 'en';
  @ViewChild(IonContent) content: IonContent;
  constructor(
    private zoharService: ZoharService,
    private sanitizer: DomSanitizer,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.appLanguage = this.translate.currentLang;
  }

  async ionViewWillEnter(){
    this.getParagraphs();
    Storage.get({ key: 'fontSize' }).then(res => {
      this.fontSize = parseInt(res.value, 0) || this.fontSize;
    });
    Storage.get({ key: 'reading_language' }).then(res => {
      this.language = res.value || this.language;
    });
    Storage.get({ key: 'show_transliteration' }).then((res: any) => {
      this.showTransComment = (res.value === 'true') || this.showTransComment;
    });
  }

  viewTransliteration(event: any){
    Storage.set({
      key: 'show_transliteration',
      value: event.detail.checked
    });
  }
  getParagraphs(onDemand = false){
    if (onDemand){
      this.loadingZohars = true;
    }
    this.nextAvailable = false;
    this.zoharService.getNextTikunim().subscribe((zohars: any) => {
      this.zohars = this.proccessParagraphs(zohars.zohars);
      this.content.scrollToTop(500);
      this.loadingZohars = false;
      setTimeout(() => {
        this.nextAvailable = true;
      }, 7000);
    }, err => {
      this.zohars = null;
    });
  }

  proccessParagraphs(zohars: any){
    zohars.Paragraphs = zohars.Paragraphs.map((par: any) => {
      if (par.Zohar.text_hebrew !== 'null') {
        par.Zohar.text_hebrew = '<span class=\'item-note\'>[' +  par.Zohar.paragraph_num + ']</span>'  + par.Zohar.text_hebrew;
      }else{
        par.Zohar.text_hebrew = null;
      }
      if (par.Zohar.text_aramaic !== 'null') {
        par.Zohar.text_aramaic = '<span class=\'item-note\'>[' +  par.Zohar.paragraph_num + ']</span>'  + par.Zohar.text_aramaic;
      }else{
        par.Zohar.text_aramaic = null;
      }
      return par;
    });
    return zohars;
  }

  fontDown() {
    if (this.fontSize > 14) {
       this.fontSize  = this.fontSize - 2;
       Storage.set({
        key: 'fontSize',
        value: this.fontSize.toString()
      });
    }
  }

  fontUp() {
    if (this.fontSize < 36) {
      this.fontSize  = this.fontSize + 2;
      Storage.set({
        key: 'fontSize',
        value: this.fontSize.toString()
      });
   }
  }

  languageChanged(ev: any) {
    const lang = ev.detail.value ?? 'hebrew';
    Storage.set({
      key: 'reading_language',
      value: lang
    });
  }
}
