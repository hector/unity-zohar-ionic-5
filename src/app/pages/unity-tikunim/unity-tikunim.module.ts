import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnityTikunimPageRoutingModule } from './unity-tikunim-routing.module';

import { UnityTikunimPage } from './unity-tikunim.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    UnityTikunimPageRoutingModule
  ],
  declarations: [UnityTikunimPage]
})
export class UnityTikunimPageModule {}
