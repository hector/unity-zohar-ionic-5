import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnityTikunimPage } from './unity-tikunim.page';

const routes: Routes = [
  {
    path: '',
    component: UnityTikunimPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnityTikunimPageRoutingModule {}
