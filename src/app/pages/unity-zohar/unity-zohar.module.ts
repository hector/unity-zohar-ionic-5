import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UnityZoharPageRoutingModule } from './unity-zohar-routing.module';

import { UnityZoharPage } from './unity-zohar.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    UnityZoharPageRoutingModule
  ],
  declarations: [UnityZoharPage]
})
export class UnityZoharPageModule {}
