import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnityZoharPage } from './unity-zohar.page';

const routes: Routes = [
  {
    path: '',
    component: UnityZoharPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnityZoharPageRoutingModule {}
