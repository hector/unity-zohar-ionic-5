import { Component, OnInit, ViewChild } from '@angular/core';
import { IonRadioGroup, LoadingController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService } from 'src/app/services/app.config.service.service';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  toggle = null;
  prefersDark = null;
  currentLang: any = null;
  mode = null;
 constructor(
   private translateService: TranslateService,
   private appConfig: AppConfigService,
   private platform: Platform,
   public loadingController: LoadingController
   ) {
 }

 ionViewWillEnter() {
    this.currentLang = this.appConfig.currentLang;
  }

  async changeLanguage() {
    const loading = await this.loadingController.create({
      backdropDismiss: true
    });
    await loading.present();
    this.translateService.use(this.currentLang).subscribe((event: any) => {
      this.appConfig.setLanguage(this.currentLang);
      loading.dismiss();
      if (this.currentLang === 'he') {
        // this.platform.setDir("rtl",true);
        this.appConfig.setDirection('rtl');
      }else{
        this.appConfig.setDirection('ltr');
        // this.platform.setDir("ltr",true);
      }
    });
  }

  async ngOnInit() {
    let prefers = 'light';
    const { value } = await Storage.get({ key: 'mode' });

    this.prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (this.prefersDark.matches){
      prefers = 'dark';
    }
    if (!value){
      this.mode = prefers;
      Storage.set({
          key: 'mode',
          value: prefers
        });
    }else{
      this.mode = value;
    }
    console.log('prefers -> ', prefers);
    this.toggle = document.querySelector('#themeToggle');
    console.log(this.toggle);

    this.toggle.addEventListener('ionChange', (ev: any) => {
      console.log('change');
      console.log(ev.detail.value);
      prefers = 'light';
      this.prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
      if (this.prefersDark.matches){
        prefers = 'dark';
      }


      if (ev.detail.value === 'light'){
        document.body.classList.remove('dark');
      }
      if (ev.detail.value === 'dark'){
        document.body.classList.add('dark');
      }

      if (ev.detail.value === 'default'){
        if (prefers === 'light'){
          document.body.classList.remove('dark');
        }
        if (prefers  === 'dark'){
          document.body.classList.add('dark');
        }
      }
      Storage.set({
        key: 'mode',
        value: ev.detail.value
      });

    });

   /* this.prefersDark.addListener((e) => this.checkToggle(e.matches));
    this.loadApp();*/
  }
  /*loadApp() {
    console.log('load');
    this.checkToggle(this.mode);
  }

  checkToggle(shouldCheck) {
    console.log('ssss');
    this.toggle.value = shouldCheck;
    this.toggle.trigger('ionChange');
  }
  */

}
