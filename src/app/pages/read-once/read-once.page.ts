import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-read-once',
  templateUrl: './read-once.page.html',
  styleUrls: ['./read-once.page.scss'],
})
export class ReadOncePage implements OnInit {
  prayTop: SafeHtml;
  constructor(
    private modalCtrl: ModalController,
    private sanitizer: DomSanitizer,
    private translate: TranslateService
    ) { }

  ngOnInit() {
  }
  dismiss() {
    this.modalCtrl.dismiss();
  }

  ionViewWillEnter(){
    this.translate.get('PRAYTOP').subscribe((res: string) => {
      this.prayTop = this.sanitizer.bypassSecurityTrustHtml(res);
    });
  }

}
