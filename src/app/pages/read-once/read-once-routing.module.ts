import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReadOncePage } from './read-once.page';

const routes: Routes = [
  {
    path: '',
    component: ReadOncePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReadOncePageRoutingModule {}
