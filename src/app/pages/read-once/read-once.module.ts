import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReadOncePageRoutingModule } from './read-once-routing.module';

import { ReadOncePage } from './read-once.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    ReadOncePageRoutingModule,
  ],
  declarations: [ReadOncePage]
})
export class ReadOncePageModule {}
