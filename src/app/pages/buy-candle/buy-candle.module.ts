import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BuyCandlePageRoutingModule } from './buy-candle-routing.module';

import { BuyCandlePage } from './buy-candle.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    BuyCandlePageRoutingModule
  ],
  declarations: [BuyCandlePage]
})
export class BuyCandlePageModule {}
