import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { InAppPurchase2, IAPProduct, IAPQueryCallback } from '@ionic-native/in-app-purchase-2/ngx';
import { AlertController, LoadingController, ModalController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ProductsService } from 'src/app/services/products.service';
import { ZoharService } from 'src/app/services/zohar.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-buy-candle',
  templateUrl: './buy-candle.page.html',
  styleUrls: ['./buy-candle.page.scss'],
})
export class BuyCandlePage implements OnInit {
  candleForm: any = null;
  products =  [];
  submitting = false;
  constructor(
    private formBuilder: FormBuilder,
    private modalCtrl: ModalController,
    private store: InAppPurchase2,
    private ref: ChangeDetectorRef,
    private platform: Platform,
    private alertController: AlertController,
    private zoharService: ZoharService,
    private translate: TranslateService,
    private loadingController: LoadingController,
    private productsService: ProductsService,
  ) {

    this.platform.ready().then(() => {
      if (this.platform.is('hybrid')){
        if (!environment.production) {
          this.store.verbosity = this.store.DEBUG;
        }
        // Get the real product information
        this.getProducts();
        this.store.ready(() => {
          this.products = this.store.products.filter((product: IAPProduct) => {
            return product.canPurchase && product.type === this.store.CONSUMABLE;
          });
          console.log(JSON.stringify(this.products));
          this.ref.detectChanges();
        });
      }
    });

  }

  ngOnInit() {
    this._buildForm();
  }

  _buildForm(){
    this.candleForm = this.formBuilder.group({
      type: new FormControl('refua'),
      gender: new FormControl('ben'),
      name: new FormControl('', [
        Validators.required
      ]),
      parent_name: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        this.emailValidator
      ]),
      user_name: new FormControl('', [
        Validators.required
      ]),
      donation: new FormControl('unity.candle_10_days', [
        Validators.required
      ])
    });
  }
/*------------------------------------------------------------*/
  get name() { return this.candleForm.get('name'); }
  get type() { return this.candleForm.get('type'); }
  get gender() { return this.candleForm.get('gender'); }
  get parent_name() { return this.candleForm.get('parent_name'); }
  get email() { return this.candleForm.get('email'); }
  get user_name() { return this.candleForm.get('user_name'); }
  get donation() { return this.candleForm.get('donation'); }
/*------------------------------------------------------------*/
  dismiss(data: any = {}) {
    this.products = [];
    this.modalCtrl.dismiss(data);
  }
/*------------------------------------------------------------*/
  emailValidator(control: any) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (!EMAIL_REGEXP.test(control.value)) {
      return {invalidEmail: true};
    }
  }
/*------------------------------------------------------------*/
  getProducts() {
    const products: any = this.productsService.getProducts();
    products.forEach((product: any) => {
      const p = this.store.get(product.id);
      this.setupListeners(product.id);
    });
    this.store.refresh();
  }
/*------------------------------------------------------------*/
  setupListeners(productId: any) {
    console.error('SETTING UP LISTENERS -- -- - - - - - -- - 2222');
    this.store.when(productId).approved(this.onProductApproved);
    this.store.when(productId).cancelled((p: IAPProduct) => {
      console.log('----- CANCELED 2', p.id);
      this.submitting = false;
      console.log(this.submitting);
      this.ref.detectChanges();
      console.log(JSON.stringify(p));
    });
  }
/*------------------------------------------------------------*/
  onProductApproved: IAPQueryCallback = (product: IAPProduct) => {
    console.log('------ APPROVED PRODUCT', this.candleForm.value.donation, product.id);
    const data = {...(this.candleForm.value)};
    data.donation = this.getDonationByProduct(data.donation);
    this.saveCandleServer(data);
    this.ref.detectChanges();
    product.finish();
    return product.verify();
  }
/*------------------------------------------------------------*/
 async purchase() {
    const data = {...(this.candleForm.value)};
    console.log(data);
    this.submitting = true;
    if (this.platform.is('hybrid')){
      console.log(JSON.stringify(this.candleForm.value));
      this.store.order(this.candleForm.value.donation).then( (p: any) => {
        console.log('------> Purchase in progress!', p);
      }, e => {
        console.error('------> Purchase in FAILED!', e);
        this.presentAlert('Failed', `Failed to purchase: ${e}`, 'OK');
      });
    }else{
      this.submitting = false;
      // data.donation = this.getDonationByProduct(data.donation);
      // await this.saveCandleServer(data);
    }
  }
/*------------------------------------------------------------*/
  async saveCandleServer(data: any){
    const texts =  this.translate.instant([
      'CANDLE_BUYED',
      'CANDLE_NOT_BUYED',
      'OK',
      'BUY_CANDLE',
      'ADDING_CANDLE'
    ]);
    let message = null;
    if (this.platform.is('ios')){
      message = texts.ADDING_CANDLE;
    }
    const loading = await this.loadingController.create({
      backdropDismiss: false,
      message
    });
    await loading.present();
    const result = await this.zoharService.buyCandle(data);
    loading.dismiss();
    if (result){
      if (!this.platform.is('ios')){
        await this.presentAlert(texts.BUY_CANDLE, texts.CANDLE_BUYED, texts.OK);
      }
      this.dismiss({
        buyed: true
      });
    }else{
      await this.presentAlert(texts.BUY_CANDLE, texts.CANDLE_NOT_BUYED, texts.OK);

    }
  }
/*------------------------------------------------------------*/
  async presentAlert(header: string, message: string, button: string) {
    const alert = await this.alertController.create({
      header,
      message,
      backdropDismiss: false,
      buttons: [button]
    });
    await alert.present();
  }
/*------------------------------------------------------------*/
  getDonationByProduct(productId: any){
    const products: any = this.productsService.getProducts();
    const donation = products.filter((product: any) => {
      return product.id === productId;
    });
    return donation[0].donation_time || 10;
  }
  ionViewDidLeave() {
    console.log('leaving');
    this.store.off(this.onProductApproved);
  }
}
