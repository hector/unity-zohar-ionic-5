import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {IonSlides} from '@ionic/angular';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { ReadOncePage } from '../read-once/read-once.page';
import { ZoharService } from 'src/app/services/zohar.service';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
import { Subject } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage  implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  slideOpts2 = {
    initialSlide: 0,
    speed: 400,
    autoHeight: true
  };
  texts: any = {};
  stats = null;
  limit = 5;
  code$ = new Subject();
  confirmBtn = null;
  re = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
  statisticsActivated = false;
  constructor(
    private sanitizer: DomSanitizer,
    private translate: TranslateService,
    private modalController: ModalController,
    private zoharService: ZoharService,
    private alertController: AlertController,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
  }
  updateView(){
    setTimeout(() => {
      this.slides.update();
    }, 500);
  }
  ionViewWillEnter(){
    this.texts =  this.translate.instant([
      'EXPLANATION',
      'ABOUT_UZ'
    ]);
    this.texts.EXPLANATION = this.sanitizer.bypassSecurityTrustHtml(this.texts.EXPLANATION);
    this.texts.ABOUT_UZ = this.sanitizer.bypassSecurityTrustHtml(this.texts.ABOUT_UZ);
    console.log(this.texts);
    /*this.translate.get('').subscribe((res: string) => {
      this.slidesText = this.sanitizer.bypassSecurityTrustHtml(res);
    });*/
    this.getStats();
    // this.loadingService.stopLoading();
  }

  async getStats(){
    this.zoharService.getGlobalStats().subscribe(async (res: any) => {
      const { value } = await Storage.get({ key: 'user_token' });
      this.statisticsActivated = value ? true : false;
      this.stats = res.stats;
    });
  }

  async presentHowWorks() {
    const modal = await this.modalController.create({
      component: ReadOncePage,
    });
    return await modal.present();
  }

  changeLimit(){
    this.limit = this.limit === 5 ? 10 : 5;
  }

  async activateStatistics(){
    const texts =  this.translate.instant([
      'ACTIVATE_PERSONAL_STATISTICS',
      'ENTER_EMAIL',
      'CANCEL',
      'OK'
    ]);

    const alert = await this.alertController.create({
      cssClass: 'alert-statistics',
      header: texts.ACTIVATE_PERSONAL_STATISTICS,
      message: texts.ENTER_EMAIL,
      inputs: [
        {
          name: 'user_email',
          type: 'email',
          id: 'user_emailid',
          placeholder: 'email@example.com',
          cssClass: 'specialClass',
        }
      ],
      buttons: [
        {
          text: texts.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('cancel Ok');
          }
        },
        {
          text: texts.OK,
          cssClass: 'confirm',
          handler: (data) => (this.activateStatisticsForUser(data))
        }
      ]
    });
    await alert.present();
    this.confirmBtn = document.querySelector('.confirm') as HTMLButtonElement;
    this.confirmBtn.disabled = true;
    const codeInput = document.getElementById('user_emailid') as HTMLInputElement;
    codeInput.addEventListener('keyup', () => this.code$.next(codeInput.value));
    this.code$.asObservable().subscribe((code: any) => { this.confirmBtn.disabled = !(this.re.test(code)); });
  }

 async  activateStatisticsForUser(alertData: any){
    const re = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    const result = re.test(alertData.user_email);
    if (result) {
      const loading = await this.loadingController.create({
        spinner: 'bubbles'
      });
      await loading.present();
      this.zoharService.activateStats(alertData.user_email).subscribe(token => {
        if (token){
          this.statisticsActivated = true;
          this.getStats();
          loading.dismiss();
          setTimeout(() => {
            this.slides.update();
          }, 500);
          return true;
        }
      }, async (err) => {
        loading.dismiss();
        console.log(err);
        const texts =  this.translate.instant([
          'ACTIVATE_PERSONAL_STATISTICS',
          'ENTER_EMAIL_FAIL',
          'CANCEL',
          'OK'
        ]);
        const alert = await this.alertController.create({
          cssClass: 'alert-statistics',
          header: texts.ACTIVATE_PERSONAL_STATISTICS,
          message: texts.ENTER_EMAIL,
          buttons: [
            {
              text: texts.OK,
              role: 'cancel',
              handler: () => {
                console.log('cancel Ok');
              }
            }]
        });
        await alert.present();
      });
    }
    return true;
  }


}
