import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

export class HttpRequestInterceptor implements HttpInterceptor {

    accessToken = null;
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!request.url.match('api')) {
            return next.handle(request);
        }
        return from(this.handle(request, next));
    }

    async handle(req: HttpRequest<any>, next: HttpHandler) {
        const params: any = {};
        if (!this.accessToken){
            const result = await Storage.get({ key: 'user_token' });
            this.accessToken = result.value || '';
            params._access_token = this.accessToken;
        }else{
            params._access_token = this.accessToken;
        }
        const lang: any = await Storage.get({ key: 'lang' });
        params._userlang = lang.value || 'en';
        const authReq = req.clone({
            setParams: params,
            setHeaders: { Authorization: 'Basic ZGV2OmlhbWRldmVsb3Blcg==' }
        });
        return next.handle(authReq).toPromise();
    }
}
