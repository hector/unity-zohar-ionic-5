import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';
const { SplashScreen, StatusBar, Storage } = Plugins;
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService } from './services/app.config.service.service';
import { LoadingService } from './services/loading.service';
import { ProductsService } from './services/products.service';
import {
  NavigationStart,
  Router,
  RouterEvent
} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  public loading = true;
  constructor(
    private platform: Platform,
    private translate: TranslateService,
    private appConfig: AppConfigService,
    private productService: ProductsService
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    this.platform.ready().then( async () => {
      if (this.platform.is('android')) {
        StatusBar.setStyle({
          style: StatusBarStyle.Dark
        }).catch( e => {
          console.warn(e);
        });
      }else{
        StatusBar.setStyle({
          style: StatusBarStyle.Light
        }).catch( e => {
          console.warn(e);
        });
      }
      await this.translateConfig();
      this.productService.init();
      const { value } = await Storage.get({ key: 'mode' });
      const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
      let prefers = 'light';
      if (prefersDark.matches){
        prefers = 'dark';
      }
      if (!value && prefers === 'dark' || value === 'dark'){
        document.body.classList.add('dark');
      }else{
        if (value === 'default' && prefers !== 'dark'){
          document.body.classList.remove('dark');
        }
        if (value === 'default' && prefers === 'dark'){
          document.body.classList.add('dark');
        }
      }
      if (!value){
        await Storage.set({
          key: 'mode',
          value: prefers
        });
      }
      console.log('huding screeen');
      SplashScreen.hide();
    });
  }

  async translateConfig() {
    let browserLang = this.translate.getBrowserLang();
    browserLang = /(es|en|ru|he)/gi.test(browserLang) ? browserLang : 'en';

    console.log('userlang', browserLang);
    this.appConfig.currentLang = browserLang;
    let lang = await this.appConfig.getLanguage();
    if (!lang) {
      lang = browserLang;
      this.translate.setDefaultLang('en');
    }
    console.log('setted lang', lang);
    await this.translate.use(lang).toPromise<void>();
    console.log('transalated');
    // this.translate.use(lang).subscribe((event: any) => {});
    await this.appConfig.setLanguage(lang);
    if (lang === 'he') {
      // document.dir = 'rtl';
    }
  }
}
