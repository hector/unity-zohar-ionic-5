import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap, catchError } from 'rxjs/operators';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
import { environment } from '../../environments/environment';
import { AlertController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  constructor(
    private http: HttpClient,
    private alertController: AlertController
    ) { }

  showAlert(msg: any) {
    const alert = this.alertController.create({
      message: msg,
      buttons: ['OK']
    });
    alert.then((res: any) => {
      return res.present();
    });
  }

  saveBookMark(data: any){
    let bookmarks = [];
    Storage.get({ key: 'bookmarks' }).then((res: any) => {
      bookmarks = JSON.parse(res.value) || [];
      if (bookmarks.length > 0){
        let found = false;
        bookmarks = bookmarks.map((bookmark) => {
          if (bookmark.zohar_id === data.zohar_id){
            found = true;
            bookmark = data;
          }
          return bookmark;
        });
        if (!found){
          bookmarks.push(data);
        }
        this.saveBookmarkServer(data).subscribe();
      }else{
        bookmarks.push(data);
        this.saveBookmarkServer(data).subscribe();
      }
      data = JSON.stringify(bookmarks);
      Storage.set({
        key: 'bookmarks',
        value: data
      });
    });
  }

  saveBookmarkServer(data: any){
    return this.http.post(environment.apiURL + 'bookmarks/save.json',
      {
        annotation: data.annotation,
        zohar_id: data.zohar_id,
        parasha_id: data.parasha_id
      }
    )
    .pipe(
      tap((res: any) => {
        if (res.bookmarks.length === 0){
          return null;
        }
        const token = res.bookmarks.Bookmark.id || null;
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );

  }

  getBookmark(zoharId: any){
    return Storage.get({ key: 'bookmarks' }).then((res) => {
      const bookmarks = JSON.parse(res.value) || [];
      let bookmark = null;
      if (bookmarks.length > 0){
        bookmark = bookmarks.filter((bookm: any) => {
          return bookm.zohar_id === zoharId;
        });
        if (bookmark.length > 0){
            return bookmark[0];
        }else{
          return null;
        }
      }else{
        return null;
      }
    });
  }

  getBookmarks(){
    return Storage.get({ key: 'bookmarks' }).then((res) => {
      return JSON.parse(res.value) || [];
    });
  }

  removeBookmark(zoharId: any){
    let bookmarks = [];
    return Storage.get({ key: 'bookmarks' }).then((res: any) => {
      bookmarks = JSON.parse(res.value) || [];
      bookmarks = bookmarks.filter((bookm: any) => {
        return bookm.zohar_id !== zoharId;
      });
      const data = JSON.stringify(bookmarks);
      Storage.set({
        key: 'bookmarks',
        value: data
      });
      this.http.post(environment.apiURL + 'bookmarks/delete.json', {
        zohar_id: zoharId,
      }).subscribe();
      return {id: zoharId, annotation: ''};
    });
  }

  saveLastRead(data: any){
    data = JSON.stringify(data);
    Storage.set({
      key: 'last_read',
      value: data
    });
  }
  getLastRead(){
    return Storage.get({ key: 'last_read' }).then((res) => {
      return JSON.parse(res.value) || null;
    });
  }

  getBookmarksFromServer(){
    return this.http.get(environment.apiURL + 'bookmarks/get.json')
    .pipe(
      tap((res: any) => {
        console.log(res);
        const data = JSON.stringify(res.bookmarks);
        Storage.set({
          key: 'bookmarks',
          value: data
        });
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }

  isBookmarked(zoharsId: any){
    return this.getBookmarks().then((bmarks: any) => {
      let founded =  bmarks.filter((bmark: any) => {
        return zoharsId.indexOf(bmark.zohar_id) !== -1;
      });
      founded = founded.map((f: any) => {
        return f.zohar_id;
      });
      return founded;
    });
  }

}
