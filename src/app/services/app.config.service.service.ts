import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { Platform } from '@ionic/angular';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  public currentLang = 'en';
  constructor(public platform: Platform) { }
  async setLanguage(lang = 'en') {
    await Storage.set({
      key: 'lang',
      value: lang
    });
    this.currentLang = lang;
  }

  async getLanguage() {
    const { value } = await Storage.get({ key: 'lang' });
    console.log('Got lang from storage: ', value);
    return value;
  }

  async setDirection(dir = 'ltr') {
    await Storage.set({
      key: 'dir',
      value: dir
    });
  }

  async getDirection() {
    const { value } = await Storage.get({ key: 'dir' });
    console.log('Got dir from storage: ', value);
    if (!value) {
      return 'rtl';
    }
    return value;
  }
}
