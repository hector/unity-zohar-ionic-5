import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { map, tap, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Injectable({
  providedIn: 'root'
})
export class ZoharService{

  constructor(
    private http: HttpClient,
    private plt: Platform,
    private alertController: AlertController
  ) { }

  showAlert(msg: any) {
    const alert = this.alertController.create({
      message: msg,
      buttons: ['OK']
    });
    alert.then((res: any) => {
      return res.present();
    });
  }
  /*---------------------------------------------------------------------------*/
  getNextZohar() {
    return this.http.get(environment.apiURL + 'zohar/next.json')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to Unity Zohar API');
          throw new Error(e);
        })
      );
  }

  getNextPinchas() {
    return this.http.get(environment.apiURL + 'zohar/next_pinchas.json')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to Unity Zohar API');
          throw new Error(e);
        })
      );
  }

  getNextTikunim() {
    return this.http.get(environment.apiURL + 'zohar/next_tikunim.json')
      .pipe(
        tap((res: any) => {
          return res;
        }),
        catchError(e => {
          console.log(e);
          this.showAlert('Cannot connect to Unity Zohar API');
          throw new Error(e);
        })
      );
  }

  getCandles(){
    return this.http.get(environment.apiURL + 'candles/get_candles.json')
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }


  getGlobalStats(){
    return this.http.get(environment.apiURL + 'zohar/stats.json')
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }

  activateStats(email: any){
    return this.http.post(environment.apiURL + 'users/activate_stats.json',
      {email}
    )
    .pipe(
      tap((res: any) => {
        const token = res.result.token || null;
        if (token){
          Storage.set({
            key: 'user_token',
            value: token
          });
        }
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }


  getBooks(){
    return this.http.get(environment.apiURL + 'parashot/get_all.json')
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }

  goToParagraph(book = '1', paragraph = '1'){
    const params = new HttpParams()
      .set('paragraph', paragraph)
      .set('book', book);

    return this.http.get(environment.apiURL + 'zohar/go_to_paragraph.json', {params})
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }


  getNextParagraphs(paragraphId: any, reverse: any = 0){
    let params = new HttpParams();
    if (reverse){
      params = params.set('reverse', reverse);
    }
    return this.http.get(environment.apiURL + 'zohar/next_paragraphs/' + paragraphId + '.json', {params})
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }

  getVisitors(){
    return this.http.get(environment.visitorsUrl + 'get_visitors/zohar/200.json')
    .pipe(
      tap((res: any) => {
        return res;
      }),
      catchError(e => {
        console.log(e);
        this.showAlert('Cannot connect to Unity Zohar API');
        throw new Error(e);
      })
    );
  }
  async buyCandle(data: any){
    return this.http.post(environment.apiURL + 'candles/register_candle.json', {data})
    .toPromise()
    .then((res: any) => {
        return res.result.saved || 0;
    }, (e) => {
      console.log(e);
      this.showAlert('Cannot connect to Unity Zohar API');
      throw new Error(e);
    });
  }
}
