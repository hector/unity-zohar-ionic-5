import { Injectable } from '@angular/core';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { Platform } from '@ionic/angular';
import { environment } from 'src/environments/environment';

const PRODUCTS = [
  {
    id: 'unity.candle_10_days',
    donation_time: 10,
    title: '10 days Candle',
    description: 'Add names to the list for the next 10 days',
    price: '10',
    currency: 'USD'
  },
  {
    id: 'unity.candle_32_days',
    donation_time: 32,
    title: '32 days Candle',
    description: 'Add names to the list for the next 32 days',
    price: '32',
    currency: 'USD'
  },
  {
    id: 'unity.candle_90_days',
    donation_time: 90,
    title: '90 days Candle',
    description: 'Add names to the list for the next 90 days',
    price: '10',
    currency: 'USD'
  },
  {
    id: 'unity.candle_180_days',
    donation_time: 180,
    title: '180 days Candle',
    description: 'Add names to the list for the next 180 days',
    price: '10',
    currency: 'USD'
  },
  {
    id: 'unity.candle_365_days',
    donation_time: 365,
    title: '365 days Candle',
    description: 'Add names to the list for the next 365 days',
    price: '10',
    currency: 'USD'
  },
];


@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  products = [];
  constructor(
    private store: InAppPurchase2,
    private platform: Platform,
  ) { }

/*--------------------------------------------------------*/
  init(){
    console.log('init porduct service');
    this.platform.ready().then(() => {
      if (this.platform.is('hybrid')){
        if (!environment.production) {
          this.store.verbosity = this.store.DEBUG;
        }
        this.registerProducts();
        this.store.refresh();
        this.store.ready(() => {
          console.log('product service: products loaded');
          this.products = this.store.products;
        });
      }
    });
  }
/*--------------------------------------------------------*/
  registerProducts() {
    PRODUCTS.forEach((product) => {
      const p = this.store.get(product.id);
      if (!p){
        console.log(product.id, ' not registered');
        this.store.register({
          id: product.id,
          type: this.store.CONSUMABLE,
        });
        this.setupListeners(product.id);
      }else{
        console.log(product.id, ' registered');
      }
    });
  }
/*--------------------------------------------------------*/
  setupListeners(productId: any) {
    console.error('SETTING UP LISTENERS ------');
    this.store.when(productId).registered( (product: IAPProduct) => {
      console.log('Registered: ', JSON.stringify(product));
    });

    this.store.when(productId).updated((p: IAPProduct) => {
      if (p.loaded && p.valid && p.state === 'approved' && p.transaction != null) {
        p.finish();
      }
    });
    this.store.when(productId).finished((p: IAPProduct) => {
        console.log('----- FINISHED', p.id);
        console.log(JSON.stringify(p));
    });
    this.store.when(productId).cancelled((p: IAPProduct) => {
      console.log('----- CANCELED', p.id);
      console.log(JSON.stringify(p));
    });
  }
/*------------------------------------------------------------*/
  getProducts(){
    return PRODUCTS;
  }

}
