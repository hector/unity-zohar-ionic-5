import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SimpleLoadingStrategy } from './Util/preload-strategy';
const routes: Routes = [
  {
    path: '',
    loadChildren: './pages/home/home.module#HomePageModule',
    data: {
      preload: true
    }
  },
  {
    path: 'home',
    loadChildren: './pages/home/home.module#HomePageModule',
    data: {
      preload: true
    }
  },
  {
    path: 'unity-zohar',
    loadChildren: './pages/unity-zohar/unity-zohar.module#UnityZoharPageModule',
  },
  {
    path: 'unity-pinchas',
    loadChildren: './pages/unity-pinchas/unity-pinchas.module#UnityPinchasPageModule',
  },
  {
    path: 'unity-tikunim',
    loadChildren: './pages/unity-tikunim/unity-tikunim.module#UnityTikunimPageModule'
  },
  {
    path: 'zohar-by-books',
    loadChildren: './pages/zohar-by-books/zohar-by-books.module#ZoharByBooksPageModule',
  },
  {
    path: 'projects',
    loadChildren: './pages/projects/projects.module#ProjectsPageModule'
  },
  {
    path: 'settings',
    loadChildren: './pages/settings/settings.module#SettingsPageModule',
  },
  {
    path: 'map',
    loadChildren: './pages/map/map.module#MapPageModule',
  },
  {
    path: 'how-works',
    loadChildren: './pages/how-works/how-works.module#HowWorksPageModule'
  },
  {
    path: 'read-once',
    loadChildren: './pages/read-once/read-once.module#ReadOncePageModule',
    data: {
      preload: true
    }
  },
  {
    path: 'buy-candle',
    loadChildren: () => import('./pages/buy-candle/buy-candle.module').then( m => m.BuyCandlePageModule),
    data: {
      preload: true
    }
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },
  // default route
  { path: '**', redirectTo: ''},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: SimpleLoadingStrategy, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
