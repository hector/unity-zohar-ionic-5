
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate//http-loader';
import { environment } from '../../environments/environment';
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, environment.apiURL + 'app_translations/get/', '.json');
}
