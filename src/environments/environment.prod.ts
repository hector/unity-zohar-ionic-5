export const environment = {
  production: true,
  apiURL: 'https://unityzohar.com/api/',
  visitorsUrl: 'https://unityzohar.com/pages/'
};
